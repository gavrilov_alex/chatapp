import {usersOnlineModel} from "./model";
import {setUserOffline, setUserOnline, setUsersLength, wsConnect} from "./logic";

export const initUsersOnlineControll  = (person) => {

    window.onunload = () => {
        setUserOffline(person);
    };

    setInterval(() => {
        setUsersLength();
    }, 1000);

    usersOnlineModel.usersLength.addEventListener('mouseover',  () => {
        usersOnlineModel.usersTable.classList.add('users__modal--active')
    });

    usersOnlineModel.usersLength.addEventListener('mouseout',  () => {
        usersOnlineModel.usersTable.classList.remove('users__modal--active')
    });

};


