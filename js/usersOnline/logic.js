import {usersOnlineModel} from "./model";

export const setUsersLength = () => {
    usersOnlineModel.xhr.open('GET', '/getUsersLength', true);
    usersOnlineModel.xhr.send();
    usersOnlineModel.xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            document.getElementById('usersLength').innerHTML = JSON.parse(usersOnlineModel.xhr.response).res;
            printOnlineUsers(JSON.parse(usersOnlineModel.xhr.response).usersList)
        }
    };
};

export const setUserOnline = (person) => {
    usersOnlineModel.xhr.open('PUT', '/userOn', true);
    usersOnlineModel.xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    usersOnlineModel.xhr.send(JSON.stringify({
        nickName : person.getNickName() ? person.getNickName() : null,
        IP: person.getIP(),
        DID: localStorage.getItem('deviceID'),
    }));
};

export const setUserOffline = (person) => {
    usersOnlineModel.xhr.open('PUT', '/userOff', true);
    usersOnlineModel.xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    usersOnlineModel.xhr.send(JSON.stringify({
        nickName : person.getNickName() ? person.getNickName() : null,
        IP: person.getIP(),
        DID: localStorage.getItem('deviceID'),
    }));
};

export const printOnlineUsers = (...args) => {
    let users = [...args];
    users = users[0];
    usersOnlineModel.setUsersUl();
    usersOnlineModel.usersUl.parentNode.removeChild(usersOnlineModel.usersUl);

    const ul = document.createElement('ul');
    ul.setAttribute('class', 'modal__ul ul');
    ul.setAttribute('id', 'table');
    for (let i = 0; i < users.length; i++) {
        const li = document.createElement('li');
        li.setAttribute('class', 'ul__li');
        li.innerHTML = users[i].nickName ? users[i].nickName : users[i].IP;
        ul.appendChild(li);
    }

    usersOnlineModel.usersTable.appendChild(ul);
};