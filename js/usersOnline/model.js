export const usersOnlineModel = {
    xhr: new XMLHttpRequest(),
    usersUl: document.getElementById('table'),
    usersLi: document.getElementsByClassName('ul__li'),
    usersTable: document.getElementById('online'),
    usersLength: document.getElementById('usersLength'),
    setUsersUl() {
        this.usersUl = document.getElementById('table')
    },
};