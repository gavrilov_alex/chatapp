document.addEventListener('DOMContentLoaded', init);

import '../style/main.less';
import {initController} from "./controller";
import {initMessagesController} from "./messages/controller";
import {initUsersOnlineControll} from "./usersOnline/controller";
import {initArrowControll} from "./scrollArrow/controller";
import {initSmilesControll} from "./smiles/controller";
import {initBtnStyle} from "./changeStyle/controller";

function init() {
    const person = initController();
    initSmilesControll();
    initArrowControll();
    initMessagesController(person);
    initUsersOnlineControll(person);
    initBtnStyle()
}